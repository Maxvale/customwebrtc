Pod::Spec.new do |s|

	s.name             = "customwebrtc"
	s.version          = "1.0.0"
	s.summary          = "Modified lib of Web RTC https://bitbucket.org/Maxvale/customwebrtc"

	s.description      = <<-DESC
	Logic behind offers for internal projects.
	DESC

	s.homepage         = "https://bitbucket.org/Maxvale/customwebrtc"
	s.license          = { :type => "BSD", :file => "LICENSE" }
	s.author           = { "Zmicier Biesau" => "zmicier.biesau@gmail.com" }
	s.source           = { :git => "https://bitbucket.org/Maxvale/customwebrtc.git", :tag => s.version.to_s }
	s.ios.source_files =  'customwebrtc/Headers/*.h'
  s.osx.source_files =  'customwebrtc/Headers/*.h'
  s.osx.public_header_files = "customwebrtc/Headers/*.h"
  s.ios.public_header_files = "customwebrtc/Headers/*.h"
  s.ios.preserve_paths = 'customwebrtc/libMeetRTCSDK.a'
  s.ios.vendored_libraries = 'customwebrtc/libMeetRTCSDK.a'
  s.ios.deployment_target = '7.0'
  s.source_files =  'customwebrtc/Headers/*.h'
  s.osx.framework = 'AVFoundation', 'AudioToolbox', 'CoreGraphics', 'CoreMedia', 'GLKit', 'CoreAudio', 'CoreVideo', 'VideoToolbox'
  s.ios.framework = 'AVFoundation', 'AudioToolbox', 'CoreGraphics', 'CoreMedia', 'GLKit', 'UIKit', 'VideoToolbox'
  s.libraries = 'c', 'sqlite3', 'stdc++'
  s.requires_arc = true
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/customwebrtc"',
                   'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Headers/customwebrtc"' }
 
end
